FROM mambaorg/micromamba

WORKDIR app/

COPY env.yml ./
RUN micromamba create -f env.yml

COPY poetry.lock pyproject.toml ./

RUN micromamba run -n homeworkslipagina poetry install --with dev

#RUN micromamba run -n homeworkslipagina poetry run kaggle datasets download -d new-york-city/ny-2015-street-tree-census-tree-data

