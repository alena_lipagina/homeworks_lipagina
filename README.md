# homeworks_lipagina

This repository created for making and checking homework from MLOps and production in DS 3.0

## Run in Docker

create Docker image:

`docker build -t hw2 .`

run docker container

`docker run --rm hw2`

## Snakemake pipeline


![dag.svg](./dag.svg)
