datasets = ["dataset_prep_low", "dataset_prep"]
models = ["random_forest", "k_neighbors"]
rule all:
   input:
      expand("models/{ds}_{m}.pkl", ds=datasets, m=models)


rule get_data:
   input: 
      "kaggle_tree_dataset.txt"
   output: 
      "dataset/orig_dataset.csv"
   script:
      "homeworkslipagina/get_data.py"

rule preprocess_data:
   input: 
      "dataset/orig_dataset.csv"
   output: 
      "dataset/dataset_prep.csv"
   script:
      "homeworkslipagina/preprocess_data.py"

rule preprocess_data_low:
   input: 
      "dataset/dataset_prep.csv"
   output: 
      "dataset/dataset_prep_low.csv"
   script:
      "homeworkslipagina/preprocess_data_low_cols.py"

rule train_k_neighbors_model:
   input: 
      "dataset/{ds}.csv"
   output: 
      "models/{ds}_k_neighbors.pkl"
   script:
      "homeworkslipagina/train_model.py"

rule train_random_forest_model:
   input: 
      "dataset/{ds}.csv"
   output: 
      "models/{ds}_random_forest.pkl"
   threads: 4
   script:
      "homeworkslipagina/train_random_forest_model.py"