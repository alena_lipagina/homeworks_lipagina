import numpy as np


def add(
    variable_1,
    variable_2,
):
    """Summ two variables

    Args:
        variable_1 (int): integer variable
        variable_2 (int): integer variable
    Returns:
        sum of variables
    """
    return variable_1 + variable_2


def hello():
    print("hello world!")
    print(np.ones((2, 2)))


# if __name__ == "__main__":
#     hello()
