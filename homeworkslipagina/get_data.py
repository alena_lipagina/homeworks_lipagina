import os

import kaggle

smk = snakemake  # noqa


def get_data_from_kaggle():
    path_to_download = "dataset"
    with open(smk.input[0], "r") as f:
        dataset_name = f.readline()
    kaggle.api.authenticate()
    kaggle.api.dataset_download_files(dataset_name, path=path_to_download, unzip=True)
    for filename in os.listdir(path_to_download):
        if filename.endswith(".csv"):
            os.rename(path_to_download + "/" + filename, smk.output[0])

    return smk.output[0]


if __name__ == "__main__":
    get_data_from_kaggle()
