import os
import pickle
import sys

import numpy as np
import pandas as pd
from hydra.utils import instantiate
from sklearn.model_selection import train_test_split

from src.config import compose_config

np.random.seed(42)
# get the project path dynamically to avoid hardcoded path
project_path = os.path.abspath(os.path.join("."))
# check the path is not already in sys.path, to avoid duplicates
if project_path not in sys.path:
    sys.path.insert(0, project_path)


main_conf = compose_config(overrides=["model='k_neighbours'"])


def train_model_k_neighbours():
    smk = snakemake  # noqa
    dataframe = pd.read_csv(
        smk.input[0],
        encoding="windows-1251",
        sep=",",
        keep_default_na=False,
        na_values=["NaN", ""],
    )
    dataframe = dataframe.sample(frac=0.3).reset_index(drop=True)
    X = dataframe.drop(["health"], axis=1)
    y = dataframe["health"]
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

    model = instantiate(main_conf["model"])
    model.fit(X_train, y_train)

    pickle.dump(model, open(smk.output[0], "wb"))


if __name__ == "__main__":
    train_model_k_neighbours()
