import os
import sys

import pandas as pd

from src.config import compose_config

# get the project path dynamically to avoid hardcoded path
project_path = os.path.abspath(os.path.join("."))
# check the path is not already in sys.path, to avoid duplicates
if project_path not in sys.path:
    sys.path.insert(0, project_path)


main_conf = compose_config()
prep_conf = main_conf["preprocessing"]["cols_to_drop"]
smk = snakemake  # noqa


def preprocess_data():
    # Read csv file as pandas DataFrame
    dataframe_orig = pd.read_csv(
        smk.input[0],
        encoding="windows-1251",
        sep=",",
        keep_default_na=False,
        na_values=["NaN", ""],
    )

    # Delete columns
    def drop_cols(df, cols_to_drop):
        return df.drop(cols_to_drop, axis=1)

    dataframe_modified = drop_cols(df=dataframe_orig, cols_to_drop=prep_conf)
    # Turning data to numeric values
    for col in [
        "root_stone",
        "root_grate",
        "root_other",
        "trunk_wire",
        "trnk_light",
        "trnk_other",
        "brch_light",
        "brch_shoe",
        "brch_other",
    ]:
        dataframe_modified[col] = dataframe_modified[col].map({"Yes": 1, "No": 0})
    dataframe_modified["curb_loc"] = dataframe_modified["curb_loc"].map({"OnCurb": 1, "OffsetFromCurb": 0})
    dataframe_modified["sidewalk"] = dataframe_modified["sidewalk"].map({"Damage": 1, "NoDamage": 0})
    dataframe_modified["status"] = dataframe_modified["status"].map({"Dead": 0, "Stump": 1, "Alive": 2})
    dataframe_modified["health"] = dataframe_modified["health"].map({"Poor": 0, "Fair": 1, "Good": 2})
    dataframe_modified["steward"] = dataframe_modified["steward"].map({"None": 0, "1or2": 1, "3or4": 2, "4orMore": 3})
    dataframe_modified["guards"] = dataframe_modified["guards"].map(
        {"None": 0, "Harmful": 1, "Unsure": 2, "Helpful": 3}
    )
    dataframe_modified = pd.get_dummies(dataframe_modified, dtype="int", columns=["user_type"])

    for label, content in dataframe_modified.items():
        if not pd.api.types.is_numeric_dtype(content):
            dataframe_modified[label] = pd.Categorical(content).codes + 1
    # Takes only alive trees
    dataframe_modified_alive = dataframe_modified.query("status==2")
    dataframe_modified_alive = dataframe_modified_alive.drop(
        [
            "stump_diam",
            "status",
        ],
        axis=1,
    )
    dataframe_modified_alive = dataframe_modified_alive.dropna()
    dataframe_modified_alive.to_csv(smk.output[0], sep=",", index=False)


if __name__ == "__main__":
    preprocess_data()
