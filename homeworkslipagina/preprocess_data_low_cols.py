import os
import sys

import pandas as pd

from src.config import compose_config

# get the project path dynamically to avoid hardcoded path
project_path = os.path.abspath(os.path.join("."))
# check the path is not already in sys.path, to avoid duplicates
if project_path not in sys.path:
    sys.path.insert(0, project_path)


main_conf = compose_config(overrides=["preprocessing='smaller_dataset'"])
prep_conf = main_conf["preprocessing"]["cols_to_drop"]
smk = snakemake  # noqa


def preprocess_data():
    # Read csv file as pandas DataFrame
    dataframe_modified = pd.read_csv(
        smk.input[0],
        encoding="windows-1251",
        sep=",",
        keep_default_na=False,
        na_values=["NaN", ""],
    )

    def drop_cols(df, cols_to_drop):
        return df.drop(cols_to_drop, axis=1)

    dataframe_modified_alive_low_cols = drop_cols(df=dataframe_modified, cols_to_drop=prep_conf)

    dataframe_modified_alive_low_cols.to_csv(smk.output[0], sep=",", index=False)


if __name__ == "__main__":
    preprocess_data()
